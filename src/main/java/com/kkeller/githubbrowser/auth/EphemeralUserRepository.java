package com.kkeller.githubbrowser.auth;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

/**
 * Dirty way of managing users using ConcurrentHashMap. "admin" user is
 * hardcoded in constructor and all other users have to be defined in users.json
 * file before starting the app. Any kind of database can be used here instead.
 */
@Repository
public class EphemeralUserRepository implements UserRepository {

	private static final Logger logger = LoggerFactory.getLogger(EphemeralUserRepository.class);

	private Long id = 0L;
	private final ConcurrentHashMap<Long, User> usersDb = new ConcurrentHashMap<>();

	public EphemeralUserRepository() throws IOException {
		addUser("admin", "pass");
		loadUsersFromUsersFile();
	}

	private void loadUsersFromUsersFile() throws IOException {
		final Map<String, String> users = JsonUsersLoader.load();
		for (final String username : users.keySet()) {
			final String password = users.get(username);
			addUser(username, password);
		}
	}

	private void addUser(String username, String password) {
		final Long currentId = id++;
		final User user = new User();
		user.setUsername(username);
		user.setPassword(new BCryptPasswordEncoder().encode(password));
		logger.info("Adding user '" + username + "' with ID: " + currentId);
		usersDb.put(currentId, user);
	}

	@Override
	public User findByUsername(String username) {
		logger.info("Looking for username '" + username + "'");
		for (final User user : usersDb.values()) {
			if (user.getUsername().equalsIgnoreCase(username)) {
				return user;
			}
		}
		return null;
	}

	@Override
	public List<User> findAll() {
		return null;
	}

	@Override
	public List<User> findAll(Sort sort) {
		return null;
	}

	@Override
	public List<User> findAllById(Iterable<Long> ids) {
		return null;
	}

	@Override
	public <S extends User> List<S> saveAll(Iterable<S> entities) {
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
	}

	@Override
	public <S extends User> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<User> entities) {
		// TODO Auto-generated method stub
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
	}

	@Override
	public User getOne(Long id) {
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<User> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> S save(S entity) {
		// TODO Auto-generated method stub
		return entity;
	}

	@Override
	public Optional<User> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(User entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll(Iterable<? extends User> entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public <S extends User> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends User> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}
}
