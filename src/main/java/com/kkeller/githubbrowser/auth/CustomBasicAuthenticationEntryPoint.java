package com.kkeller.githubbrowser.auth;

import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class CustomBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {
	public CustomBasicAuthenticationEntryPoint() {
		setRealmName("github-browser");
	}
}