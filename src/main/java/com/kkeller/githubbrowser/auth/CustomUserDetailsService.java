package com.kkeller.githubbrowser.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository h2UserRepository;
	@Autowired
	private UserRepository ephemeralUserRepository;
	@Value("${githubbrowser.db.type:ephemeral}")
	String dbType;

	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) {
		setupUserRepository();
		final User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return new CustomUserDetails(user);
	}

	private void setupUserRepository() {
		if (userRepository == null) {
			switch (dbType) {
				case "h2":
					userRepository = h2UserRepository;
					break;
				case "ephemeral":
				default:
					userRepository = ephemeralUserRepository;
			}
		}
	}
}
