package com.kkeller.githubbrowser.auth;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class JsonUsersLoader {

	// TODO Should be configurable?
	private static final String USERS_PATH = "/users.json";

	public static Map<String, String> load() throws IOException {
		// Failing in .jar. Left only to don't forget about it.
//		final File file = ResourceUtils.getFile(USERS_PATH);
//		final String usersJson = new String(Files.readAllBytes(Paths.get(file.getAbsolutePath())),
//				StandardCharsets.UTF_8);
//		final String usersJson = readFromFile(file);
		final InputStream fileIS = JsonUsersLoader.class.getResourceAsStream(USERS_PATH);
		final String usersJson = readFromIS(fileIS);
		return new Gson().fromJson(usersJson, new TypeToken<Map<String, String>>() {
		}.getType());
	}

	private static String readFromIS(InputStream inputStream) throws IOException {
		final ByteArrayOutputStream result = new ByteArrayOutputStream();
		final byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer)) != -1) {
			result.write(buffer, 0, length);
		}
		return result.toString(StandardCharsets.UTF_8.name());
	}

//	private static String readFromFile(File file) throws FileNotFoundException, IOException {
//		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
//			final StringBuilder sb = new StringBuilder();
//			String line = br.readLine();
//			while (line != null) {
//				sb.append(line);
//				sb.append(System.lineSeparator());
//				line = br.readLine();
//			}
//			return sb.toString();
//		}
//	}
}
