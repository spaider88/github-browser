package com.kkeller.githubbrowser.auth;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class H2UserRepository implements UserRepository {

	private static final Logger logger = LoggerFactory.getLogger(H2UserRepository.class);

	private static final String TABLE_NAME = "users";

	private final JdbcTemplate jdbcTemplate;

	public H2UserRepository(JdbcTemplate jdbcTemplate) throws IOException {
		this.jdbcTemplate = jdbcTemplate;
		deleteAllInBatch();
		addUser("admin", "pass");
		loadUsersFromUsersFile();
	}

	private void addUser(String username, String password) {
		logger.info("Adding user '" + username + "'");
		final GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update((PreparedStatementCreator) connection -> {
			final PreparedStatement statement = connection.prepareStatement("INSERT INTO " + TABLE_NAME
					+ " (username, password) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, username);
			statement.setString(2, new BCryptPasswordEncoder().encode(password));
			return statement;
		}, keyHolder);
		logger.info("User '" + username + "' added with ID " + keyHolder.getKey());
	}

	private void loadUsersFromUsersFile() throws IOException {
		final Map<String, String> users = JsonUsersLoader.load();
		for (final String username : users.keySet()) {
			final String password = users.get(username);
			addUser(username, password);
		}
	}

	@Override
	public User findByUsername(String username) {
		logger.info("Looking for username '" + username + "'");
		return jdbcTemplate.queryForObject("SELECT * FROM " + TABLE_NAME + "  WHERE username=?", new Object[] {
				username }, new BeanPropertyRowMapper<User>(User.class));
	}

	@Override
	public void deleteAllInBatch() {
		jdbcTemplate.update("DELETE FROM " + TABLE_NAME);
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub

	}

	@Override
	public <S extends User> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteInBatch(Iterable<User> entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public User getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<User> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<User> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(User entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll(Iterable<? extends User> entities) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub

	}

	@Override
	public <S extends User> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends User> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends User> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}
}
