package com.kkeller.githubbrowser;

import java.util.Map;

public class ErrorResponse {

	public int status;
	public String error;
	public String message;
	public String timestamp;

	public ErrorResponse(int status, Map<String, Object> errorAttributes) {
		this.status = status;
		error = errorAttributes.get("error").toString();
		message = errorAttributes.get("message").toString();
		timestamp = errorAttributes.get("timestamp").toString();
	}
}
