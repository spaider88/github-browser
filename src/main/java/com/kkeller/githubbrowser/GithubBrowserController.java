package com.kkeller.githubbrowser;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kkeller.githubbrowser.cache.UserRepositoriesRepository;
import com.kkeller.githubbrowser.githubclient.Repository;
import com.kkeller.githubbrowser.githubclient.RepositorySerializer;
import com.kkeller.githubbrowser.githubclient.SortDirection;
import com.kkeller.githubbrowser.githubclient.UserRepositories;
import com.kkeller.githubbrowser.validators.Sort;

@SpringBootApplication
@RestController
@Validated
public class GithubBrowserController {

	@Autowired
	private UserRepositoriesRepository userRepositoriesRepository;

	// No authentication required
	@RequestMapping(method = RequestMethod.GET, path = "/ping", produces = MediaType.TEXT_PLAIN_VALUE)
	String ping() {
		return "pong";
	}

	// To redirect everything else than ping to browse
	@RequestMapping("/**/{path:[^\\\\.]+}")
	void root(HttpServletResponse response) throws IOException {
		response.sendRedirect("/browse");
	}

	@RequestMapping(method = RequestMethod.GET, path = "/browse", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<?> browse(@NotBlank @RequestParam("user") String user,
			@Nullable @Sort @RequestParam("direction") String direction,
			@Nullable @RequestParam("lastly_updated_only") Boolean lastlyUpdatedOnly) throws IOException {
		try {
			final UserRepositories userRepositories = new UserRepositories(
					userRepositoriesRepository.getUserRepositories(user));
			if (lastlyUpdatedOnly != null) {
				if (lastlyUpdatedOnly) {
					userRepositories.removeInactiveRepositories();
				} else {
					userRepositories.removeActiveRepositories();
				}
			}
			userRepositories.sortByName(SortDirection.parse(direction));
			final GsonBuilder gson = new GsonBuilder();
			gson.registerTypeAdapter(Repository.class, new RepositorySerializer());
			final Gson parser = gson.create();
			return new ResponseEntity<String>(parser.toJson(userRepositories.getRepositories()), HttpStatus.OK);
		} catch (final HttpClientErrorException e) {
			if (e.getRawStatusCode() == HttpStatus.NOT_FOUND.value()) {
				throw new Errors.UserNotFoundException();
			}
			throw e;
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(GithubBrowserController.class, args);
	}
}
