package com.kkeller.githubbrowser;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

@Controller
public class CustomErrorController implements ErrorController {

	private static final String PATH = "/error";

	@Autowired
	private ErrorAttributes errorAttributes;

	@RequestMapping(value = PATH)
	ResponseEntity<ErrorResponse> error(HttpServletRequest request, HttpServletResponse response) {
		return new ResponseEntity<ErrorResponse>(new ErrorResponse(response.getStatus(), getErrorAttributes(request)),
				HttpStatus.resolve(response.getStatus()));
	}

	private Map<String, Object> getErrorAttributes(HttpServletRequest request) {
		final WebRequest requestAttributes = new ServletWebRequest(request);
		return errorAttributes.getErrorAttributes(requestAttributes, true);
	}

	@Override
	public String getErrorPath() {
		return PATH;
	}
}
