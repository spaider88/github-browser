package com.kkeller.githubbrowser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.kkeller.githubbrowser.cache.UserRepositoriesRepository;

@Component
public class AppRunner implements CommandLineRunner {

	private static final Logger logger = LoggerFactory.getLogger(AppRunner.class);

	private final UserRepositoriesRepository userRepositoriesRepository;

	public AppRunner(UserRepositoriesRepository userRepositoriesRepository) {
		this.userRepositoriesRepository = userRepositoriesRepository;
	}

	@Override
	public void run(String... args) throws Exception {
//		final int MAX_ITER_NO = 3;
//		logger.info(".... Fetching repositories using cache ....");
//		for (int iter = 0; iter < MAX_ITER_NO; iter++) {
//			logger.info("spaider88 -->" + userRepositoriesRepository.getUserRepositories("spaider88"));
//			logger.info("zalewa -->" + userRepositoriesRepository.getUserRepositories("zalewa"));
//		}
//		logger.info(".... Fetching repositories using just githubapi ....");
//		final GithubClient client = new GithubClient();
//		for (int iter = 0; iter < MAX_ITER_NO; iter++) {
//			logger.info("spaider88 -->" + client.getUserRepositories("spaider88"));
//			logger.info("zalewa -->" + client.getUserRepositories("zalewa"));
//		}
	}
}