package com.kkeller.githubbrowser.githubclient;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepositories implements Serializable {
	private static final long serialVersionUID = 1504300114403075701L;

	private String username;
	private List<Repository> repositories;

	public UserRepositories(UserRepositories source) {
		username = source.username;
		repositories = new ArrayList<Repository>(source.repositories);
	}

	public UserRepositories(String user, List<Repository> repositories) {
		username = user;
		this.repositories = repositories;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<Repository> getRepositories() {
		return repositories;
	}

	public void setRepositories(List<Repository> repositories) {
		this.repositories = repositories;
	}

	public void removeInactiveRepositories() {
		removeRepositoriesByActivity(true);
	}

	public void removeActiveRepositories() {
		removeRepositoriesByActivity(false);
	}

	private void removeRepositoriesByActivity(boolean active) {
		repositories = repositories.stream().filter(repository -> !(active ^ repository.isLastlyUpdated())).collect(
				Collectors.toList());
	}

	/**
	 * To speed up we can move ASC after DESC and make it NOOP because we are using
	 * deep copies which use ASC ordered repositories by default
	 * ({@link com.kkeller.githubbrowser.githubclient.GithubClient#getUserRepositories })
	 **/
	public void sortByName(SortDirection direction) {
		switch (direction) {
			case ASC:
				repositories = repositories.stream().sorted((r1, r2) -> r1.getName().compareToIgnoreCase(
						r2.getName())).collect(Collectors.toList());
				break;
			case DESC:
				repositories = repositories.stream().sorted((r1, r2) -> -r1.getName().compareToIgnoreCase(
						r2.getName())).collect(Collectors.toList());
				break;
			default:
		}
	}
}
