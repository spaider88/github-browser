package com.kkeller.githubbrowser.githubclient;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class RepositorySerializer implements JsonSerializer<Repository> {

	@Override
	public JsonElement serialize(Repository repository, Type type, JsonSerializationContext context) {
		final JsonObject tree = (JsonObject) new Gson().toJsonTree(repository);
		tree.addProperty("lastly_updated", repository.isLastlyUpdated());
		return tree;
	}
}
