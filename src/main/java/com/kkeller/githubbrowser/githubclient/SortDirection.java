package com.kkeller.githubbrowser.githubclient;

public enum SortDirection {
	ASC, DESC;

	public static SortDirection parse(String value) {
		if (value == null) {
			return SortDirection.ASC;
		}
		return SortDirection.valueOf(value.toUpperCase());
	}
}
