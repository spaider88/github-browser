package com.kkeller.githubbrowser.githubclient;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import com.kkeller.githubbrowser.PropertiesLoader;

public class Repository implements Serializable {
	private static final long serialVersionUID = -2274748731509631808L;

	private String name;
	private String language;
	private Date created_at;
	private Date updated_at;

	private static final int LAST_UPDATED_IN_MONTHS = 3;
	private static final int MONTHS_IN_YEAR = 12;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Date getCreatedAt() {
		return created_at;
	}

	public void setCreatedAt(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdatedAt() {
		return updated_at;
	}

	public void setUpdatedAt(Date updated_at) {
		this.updated_at = updated_at;
	}

	public boolean isLastlyUpdated() {
		final Period period = Period.between(toLocalDate(getUpdatedAt()), LocalDate.now());
		final int yearsMonths = period.getYears() * MONTHS_IN_YEAR;
		try {
			return period.getMonths() + yearsMonths <= getLastUpdatedInMonthsFromProperties();
		} catch (IOException | NumberFormatException e) {
			return period.getMonths() + yearsMonths <= LAST_UPDATED_IN_MONTHS;
		}
	}

	private Integer getLastUpdatedInMonthsFromProperties() throws IOException {
		return Integer.valueOf(PropertiesLoader.loadProperties("application.properties").getProperty(
				"githubbrowser.lastupdatedinmonths"));
	}

	private LocalDate toLocalDate(Date date) {
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	@Override
	public String toString() {
		return "Repository [name=" + name + ", language=" + language + ", created_at=" + created_at + ", updated_at="
				+ updated_at + ", lastly_updated=" + isLastlyUpdated() + "]";
	}
}
