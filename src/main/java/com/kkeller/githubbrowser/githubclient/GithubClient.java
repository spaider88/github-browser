package com.kkeller.githubbrowser.githubclient;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Component
public class GithubClient {

	private static final Logger logger = LoggerFactory.getLogger(GithubClient.class);

	private static final String USER_AGENT = "GithubClient/0.0.1";
	private static final String ACCEPT = "application/vnd.github.v3+json";
	private static final String SERVER_URL = "https://api.github.com";
	private static final String API_PATH = "/users/{user}/repos";

	private static final String SORT_DIRECTION_PARAM_NAME = "direction";

	private final RestTemplate githubRest;
	private final HttpHeaders headers;
	private HttpStatus status;

	private final Gson gson;

	public GithubClient() {
		githubRest = new RestTemplate();
		headers = new HttpHeaders();
		headers.add("User-Agent", USER_AGENT);
		headers.add("Accept", ACCEPT);
		gson = new Gson();
	}

	public UserRepositories getUserRepositories(final String user, SortDirection direction) {
		logger.info("Fetching " + user + " repositories");
		final URI uri = buildUri(user, direction);
		return fetchUserRepositories(user, uri);
	}

	private URI buildUri(final String user, SortDirection direction) {
		final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(SERVER_URL + API_PATH);
		if (direction != null) {
			uriBuilder.queryParam(SORT_DIRECTION_PARAM_NAME, direction.toString().toLowerCase());
		}
		return uriBuilder.build(user);
	}

	private UserRepositories fetchUserRepositories(final String user, final URI uri) {
		final HttpEntity<String> requestEntity = new HttpEntity<String>("", headers);
		final ResponseEntity<String> responseEntity = githubRest.exchange(uri, HttpMethod.GET, requestEntity,
				String.class);
		setStatus(responseEntity.getStatusCode());
		final String jsonResponse = responseEntity.getBody();
		final List<Repository> repositories = gson.fromJson(jsonResponse, new TypeToken<ArrayList<Repository>>() {
		}.getType());
		return new UserRepositories(user, repositories);
	}

	public UserRepositories getUserRepositories(final String user) {
		return getUserRepositories(user, null);
	}

	public HttpStatus getStatus() {
		return status;
	}

	private void setStatus(final HttpStatus status) {
		this.status = status;
	}
}
