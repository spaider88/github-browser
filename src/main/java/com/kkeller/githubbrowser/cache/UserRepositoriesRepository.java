package com.kkeller.githubbrowser.cache;

import org.springframework.stereotype.Repository;

import com.kkeller.githubbrowser.githubclient.UserRepositories;

@Repository
public interface UserRepositoriesRepository {
	UserRepositories getUserRepositories(String user);
}
