package com.kkeller.githubbrowser.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
@EnableCaching
@EnableScheduling
public class CacheConfig {

	private static final Logger logger = LoggerFactory.getLogger(CacheConfig.class);

	// TODO Should be configurable.
	private static final long TTL = 30 * 1000;

	@CacheEvict(allEntries = true, value = { EphemeralUserRepositoriesRepository.CACHE_NAME })
	@Scheduled(fixedDelay = TTL, initialDelay = 500)
	public void reportCacheEvict() {
		logger.info("Flushing cache");
	}
}
