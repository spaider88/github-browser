package com.kkeller.githubbrowser.cache;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.kkeller.githubbrowser.githubclient.GithubClient;
import com.kkeller.githubbrowser.githubclient.UserRepositories;

/**
 * Default ConcurrentHashMap cache. Instead of tricky cache evicting in
 * {@link CacheConfig} Redis cache can be used.
 */
@Component
public class EphemeralUserRepositoriesRepository implements UserRepositoriesRepository {

	public static final String CACHE_NAME = "userRepositories";

	@Override
	@Cacheable(CACHE_NAME)
	public UserRepositories getUserRepositories(String user) {
		return new GithubClient().getUserRepositories(user);
	}
}
