package com.kkeller.githubbrowser.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SortValidator implements ConstraintValidator<Sort, String> {
	@Override
	public boolean isValid(String sort, ConstraintValidatorContext context) {
		return sort == null || sort.equalsIgnoreCase("asc") || sort.equalsIgnoreCase("desc");
	}
}
