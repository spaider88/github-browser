package com.kkeller.githubbrowser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
	public static Properties loadProperties(String resourceFileName) throws IOException {
		final Properties configuration = new Properties();
		final InputStream inputStream = PropertiesLoader.class.getClassLoader().getResourceAsStream(resourceFileName);
		configuration.load(inputStream);
		inputStream.close();
		return configuration;
	}
}