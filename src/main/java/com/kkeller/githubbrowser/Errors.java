package com.kkeller.githubbrowser;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class Errors {
	@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User not found")
	public static class UserNotFoundException extends RuntimeException {
		private static final long serialVersionUID = 4167322086140146274L;
	}
}
