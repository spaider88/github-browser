package com.kkeller.githubbrowser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.servlet.ServletContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GithubBrowserControllerTests {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Before
	public void setup() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void contextLoads() {
		final ServletContext servletContext = wac.getServletContext();
		Assert.assertNotNull(servletContext);
		Assert.assertTrue(servletContext instanceof MockServletContext);
		Assert.assertNotNull(wac.getBean("githubBrowserController"));
	}

	@Test
	public void ping() throws Exception {
		mockMvc.perform(get("/ping")).andDo(print()).andExpect(status().isOk()).andExpect(content().string("pong"));
	}

	@Test
	public void browseWithNoUserParam() throws Exception {
		final MvcResult mvcResult = mockMvc.perform(get("/browse")).andDo(print()).andExpect(
				status().isBadRequest()).andReturn();
		final String errMsg = mvcResult.getResponse().getErrorMessage();
		Assert.assertEquals(errMsg, "Required String parameter 'user' is not present");
	}

	@Test
	public void browseNotExistingUser() throws Exception {
		mockMvc.perform(get("/browse").param("user", "notexistingspaider88account")).andDo(print()).andExpect(
				status().isNotFound());
	}

	@Test
	public void browse() throws Exception {
		mockMvc.perform(get("/browse").param("user", "spaider88")).andDo(print()).andExpect(status().isOk());
	}
}
