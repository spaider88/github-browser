
# github-browser

## Introdution

For now, one and only task for **github-browser** is exposing an endpoint which will allow users to fetch all public github repositories for specified user. Additionally users can filter out the result according to passed optional *lastly_updated_only* parameter and sort it using *direction* parameter.

## Requirements

 - Java 9+

## Optional requirements

 - Redis
 - H2

## How to run

 1. `git clone
    https://spaider88@bitbucket.org/spaider88/github-browser.git`
 2. `cd github-browser`
 3. Windows: `mvnw.cmd spring-boot:run`
Unix: `mvnw spring-boot:run`

## Building
| target| cmd |
|--|--|
| jar | `mvnw package` |
| docker image | `mvnw install dockerfile:build` |


## API
Service availability:
> http(s)://HOST:PORT/ping

Target endpoint:

> http(s)://HOST:PORT/browse

### Parameters

 - **user** (required) - github username
 - **direction** (optional) - user's projects sorting order; available values: **asc**, **desc**; Default: **asc**
 - **lastly_updated_only** (optional) - show only lastly updated repositories; Default: **null**
 
### Response

JSON formatted user's repositories list.
Available user's repositories keys:

 - name
 - language
 - created_at
 - updated_at
 - lastly_updated

## Authorization
**Basic access authentication**. Authorized users are kept in memory or H2 database. By default **admin:pass** account is registered, All other users are defined in *users.json* file.

## Environment variables
 - CACHE_TYPE - *simple|redis* - simple by default
 - DB_TYPE - *ephemeral|h2* - ephemeral by default
